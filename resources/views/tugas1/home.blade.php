@extends('layout.master')

@section('judul')
Media Online
@endsection

@section('content')
	Sosial Media Developer
	<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

	<h4>Benefit Join di Media Online</h4>
	<ul>
		<li>Mendapatkan motivasi dari sesama para Developer</li>
		<li>Sharing Knowledge</li>
		<li>Dibuat oleh calon web developer terbaik</li>
	</ul>

	<h4>Cara bergabung ke Media Online</h4>

	<ol>
		<li>Mengunjungi website ini</li>
		<li>Mendaftarkan di <a href='/register'>Form Sign Up</a></li>
		<li>Selesai</li>
	</ol>
@endsection