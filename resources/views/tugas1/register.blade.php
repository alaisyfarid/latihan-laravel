@extends('layout.master')

@section('judul')
Sign Up Form
@endsection

@section('content')

	<h1>Buat Account Baru !</h1>

	<h4>Sign Up Form</h4>

	
	<form action="/welcome" method="post">
		@csrf
		<label>First Name :</label><br><br>
		<input type="text" name="namadep"><br><br>
		<label>Last Name :</label><br><br>
		<input type="text" name="namabel"><br><br>
		<label>Gender :</label>
		<br><br>
		<input type="radio" id="Male" name="kelamin" value="Male">
		<label for=Male>Male</label> <br>
		<input type="radio" id="Female" name="kelamin" value="Female">
		<label for="Female">Female</label> <br>
		<input type="radio" id="Other" name="kelamin" value="Other">
		<label for="Other">Other</label> <br><br>
		<label>Nationality :</label><br><br>
		<select name="nations" id="nations">\
			<option value="idn">Indonesia</option>
			<option value="my">Malaysia</option>
			<option value="sg">Singapore</option>
		</select><br><br>

		<label>Language Spoken :</label><br><br>
		<input type="checkbox" id="idn" name="bahasa" value="Bahasa Indonesia">
		<label for="idn">Bahasa Indonesia</label><br>
		<input type="checkbox" id="my" name="melayu" value="Melayu">
		<label for="my">Melayu</label><br>
		<input type="checkbox" id="eng" name="english" value="English">
		<label for="eng">English</label><br><br>

		<label>Bio :</label><br><br>

		<textarea id="bio" name="bio" rows="4" cols="59"></textarea><br>
		<input type="submit" value="Kirim" >

	</form>

@endsection